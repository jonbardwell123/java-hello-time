package hello;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.Date;

@RestController
public class HelloController {

//public static void main (String[] args) {

    @RequestMapping("/hello")
    public String index() {
        return "Greetings from Spring Boot!";
    }

	@RequestMapping("/get_time")
    public String get_time() {
		long millis=System.currentTimeMillis();
		java.util.Date date=new java.util.Date(millis);
        return date.toString();
	}
}
